# This file will be patched by setup.py
# The __version__ should be set to the branch name
# Leave __baseline__ set to unknown to enable setting commit-hash
# (e.g. "develop" or "1.2.x")

# You MUST use double quotes (so " and not ')
# Do not forget to update the appdata file for every major release!

__version__ = "4.4.1"
__baseline__ = "d897936da564ee5564a697dfcfd5eec5824b0d0c"
