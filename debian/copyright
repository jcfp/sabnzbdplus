Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SABnzbd
Upstream-Contact:
 The SABnzbd-Team (sabnzbd.org)
 https://forums.sabnzbd.org/
 https://github.com/sabnzbd/sabnzbd
 https://www.transifex.com/sabnzbd/sabnzbd-translations/
Source: https://sabnzbd.org/downloads
Comment: Upstream release contain a number of minified javascript
 files, used as part of the program's web interface. Non-minified
 copies are maintained in the 'missing-sources' branch of the
 packaging git repository, and added to the tarball using the
 get-orig-source target in debian/rules to create the orig.tar.gz
 for new upstream releases.
Disclaimer: This package is in contrib because it depends on
 (non-free) unrar. SABnzbd itself is entirely free and open-source
 software.

Files: *
Copyright: 2007-2025 The SABnzbd-Team (sabnzbd.org)
License: GPL-2+

Files: sabnzbd/utils/certgen.py
Copyright:
 2001 Martin Sjögren and AB Strakt,
 2008 Jean-Paul Calderone
License: LGPL-2.1+

Files: sabnzbd/utils/kronos.py
Copyright: 2004 Irmen de Jong
License: Expat

Files: sabnzbd/utils/rarfile.py
Copyright: 2005-2016 Marko Kreen <markokr@gmail.com>
License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: sabnzbd/utils/systrayiconthread.py
Copyright: 2005 Simon Brunning <simon@brunningonline.net>
Comment: with modifications by Jan Schejbal and The SABnzbd-Team.
License: Other-WTFPL-variant
 "On http://www.brunningonline.net/simon/blog/archives/001835.html,
 the author licensed SysTrayIcon.py under a variant of the WTFPL:
 .
 > Any road up, help yourself. Consider SysTrayIcon.py to be under an
 > "Aleister Crowley" style license - "Do what thou wilt shall be the
 > only law".
 >
 > Err, but don't sue me if it doesn't work. ;-)"

Files: sabnzbd/utils/pybonjour.py
Copyright: 2007-2008 Christopher J. Stawarz <cstawarz@gmail.com>
License: Expat

Files:
 tools/msgfmt.py
 sabnzbd/utils/pystone.py
Copyright: Python Software Foundation
Comment:
 "pystone.py and msgfmt.py have been copied from the Python sourcecode.
 They are covered by the same license as Python itself"
License: PSF

Files: interfaces/Config/templates/staticcfg/js/script.js
Copyright:
 2014 Bass Jobsen @bassjobsen
 2006 M. Alsup
 2017 Kevin Morris
 2012, 2015 the SABnzbd Team
 2016 Lars Jung
 2009, 2011 Kazuhiko Arase
License: Apache-2 and Expat and Expat or GPL-2
Comment:
 This file contains code from various sources:
 "bootstrap3-typeahead.js"
   Copyright: 2014 Bass Jobsen @bassjobsen
   Origin: https://github.com/bassjobsen/Bootstrap-3-Typeahead
   License: Apache-2
 "jQuery Form Plugin"
   Copyright:
    2006 M. Alsup
    2017 Kevin Morris
   Origin: https://github.com/malsup/form
   License: dual-licensed Expat, GPL-2
 "jQuery PathBrowser"
   Copyright: 2012, 2015 the SABnzbd Team
   License: dual-licensed Expat, GPL-2
 "jQuery.qrcode"
   Copyright:
    2016 Lars Jung
    2009, 2011 Kazuhiko Arase
   Origin: https://larsjung.de/jquery-qrcode/
   License: Expat

Files: interfaces/Config/templates/staticcfg/js/src/bootstrap3-typeahead_4.0.2.js
Copyright: 2014 Bass Jobsen @bassjobsen
License: Apache-2

Files: interfaces/Config/templates/staticcfg/js/src/jquery.form.js_4.2.2.js
Copyright:
 2006 M. Alsup
 2017 Kevin Morris
License: Expat or LGPL-2.1+

Files: interfaces/Config/templates/staticcfg/js/src/jquery-qrcode_0.14.0.js
Copyright:
 2009, 2011 Kazuhiko Arase
 2016 Lars Jung
License: Expat

Files:
 interfaces/Config/templates/staticcfg/js/jquery-3.5.1.min.js
 interfaces/Config/templates/staticcfg/js/src/jquery-3.5.1.js
Copyright: 2020 JS Foundation and other contributors
License: Expat


Files: interfaces/Config/templates/staticcfg/bootstrap/*
Copyright: 2011-2016 Twitter, Inc.
License: Expat

Files:
 interfaces/Config/templates/staticcfg/js/jquery.tablesort.min.js
 interfaces/Config/templates/staticcfg/js/src/jquery.tablesort_0.0.11.js
Copyright:
 2012 Kyle Fox
Comment: origin https://github.com/kylefox/jquery-tablesort
License: Expat

Files:
 interfaces/Config/templates/staticcfg/js/chartist.min.js
 interfaces/Config/templates/staticcfg/js/src/chartist_0.11.0.js
Copyright: 2017 Gion Kunz
License: Expat or WTFPL

Files:
 interfaces/Config/templates/staticcfg/js/filesize.min.js
 interfaces/Config/templates/staticcfg/js/src/filesize_3.5.11.js
Copyright: 2017 Jason Mulligan <jason.mulligan@avoidwork.com>
Comment: origin: https://github.com/avoidwork/filesize.js
License: BSD-3-Clause
 Copyright (c) 2017, Jason Mulligan
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of filesize nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: interfaces/Glitter/templates/static/javascripts/date.min.js
Copyright: 2015 Gregory Wild-Smith <gregory@wild-smith.com>
Comment: origin: https://github.com/abritinthebay/datejs
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/jquery-3.5.1.min.js
 interfaces/Glitter/templates/static/javascripts/src/jquery-3.5.1.js
Copyright: 2020 JS Foundation and other contributors
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/jquery-ui.min.js
 interfaces/Glitter/templates/static/javascripts/src/jquery-ui_1.12.1.js
Copyright: 2016 jQuery Foundation and other contributors
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/jquery.peity.min.js
 interfaces/Glitter/templates/static/javascripts/src/jquery.peity_3.3.0.js
Copyright: 2018 Ben Pickles
Comment: origin: http://benpickles.github.io/peity
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/knockout-3.5.1.min.js
 interfaces/Glitter/templates/static/javascripts/src/knockout-3.5.1.js
Copyright: 2019 The Knockout.js team
Comment: origin: http://knockoutjs.com/
License: Expat

Files: interfaces/Glitter/templates/static/javascripts/knockout-extensions.js
Copyright:
 2013 Mikael Brassman (with modifications by Safihre@sabnzbd.org)
 2019 Ryan Niemeyer
 2013 Steven Sanderson, Roy Jacobs
License: Expat
Comment:
 This file contains code from a number of different sources:
 "Knockout Persist"
   Copyright:  2013 Mikael Brassman (with modifications by Safihre@sabnzbd.org)
   Origin: https://github.com/spoike/knockout.persist
   License: Expat
 "knockout-sortable"
   Copyright: 2015 Ryan Niemeyer
   License: Expat
 "Knockout Mapping plugin"
   Copyright: 2013 Steven Sanderson, Roy Jacobs
   Origin: https://github.com/SteveSanderson/knockout.mapping
   License: Expat

Files: interfaces/Glitter/templates/static/javascripts/src/knockout.mapping_2.4.1.js
Copyright: 2013 Steven Sanderson, Roy Jacobs
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/moment-2.26.0.min.js
 interfaces/Glitter/templates/static/javascripts/src/moment-2.26.0.js
Copyright: 2020 Tim Wood, Iskren Chernev, Moment.js contributors
License: Expat

Files:
 interfaces/Glitter/templates/static/javascripts/jquery.hotkeys.min.js
 interfaces/Glitter/templates/static/javascripts/src/jquery.hotkeys.js
Copyright: 2010, John Resig
License: Expat or GPL-2

Files: interfaces/Glitter/templates/static/bootstrap/*
Copyright: 2011-2015 Twitter, Inc.
License: Expat

Files: linux/org.sabnzbd.sabnzbd.appdata.xml
Copyright: 2022-2023 The SABnzbd-Team (sabnzbd.org)
License: Expat

Files: debian/*
Copyright: 2007-2025 Jeroen Ploemen <jcfp@debian.org>
License: GPL-2+

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License
 .
 On Debian GNU/Linux systems, the complete text of the Apache License,
 version 2, can be found in `/usr/share/common-licenses/Apache-2.0'.

License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License V2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License, version 2, can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License, version 2, can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Lesser General
 Public License, version 2.1, can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: PSF
 PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
 --------------------------------------------
 .
 1. This LICENSE AGREEMENT is between the Python Software Foundation
 ("PSF"), and the Individual or Organization ("Licensee") accessing and
 otherwise using this software ("Python") in source or binary form and
 its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, PSF hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use Python alone or in any derivative version,
 provided, however, that PSF's License Agreement and PSF's notice of copyright,
 i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
 Python Software Foundation; All Rights Reserved" are retained in Python alone or
 in any derivative version prepared by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on
 or incorporates Python or any part thereof, and wants to make
 the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Python.
 .
 4. PSF is making Python available to Licensee on an "AS IS"
 basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
 A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
 OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Python, Licensee
 agrees to be bound by the terms and conditions of this License
 Agreement.

License: WTFPL
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004
 .
 Copyright: 2004 Sam Hocevar <sam@hocevar.net>
 .
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
  0. You just DO WHAT THE FUCK YOU WANT TO.
