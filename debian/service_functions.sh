# Helper functions for the init.d and systemd service of sabnzbdplus
#
# Copyright (C) 2008-2024 by Jeroen Ploemen <linux@jcf.pm>
# released under GPL, version 2 or later

parse_settings() {
	[ -n "$CONFIG" ] && OPTIONS="--config-file \"$CONFIG\"" || OPTIONS=
	[ -n "$HOST" ] && SERVER="$HOST" || SERVER=
	[ -n "$PORT" ] && SERVER="$SERVER:$PORT"
	[ -n "$SERVER" ] && OPTIONS="$OPTIONS --server \"$SERVER\""
	[ -n "$EXTRAOPTS" ] && OPTIONS="$OPTIONS $EXTRAOPTS"

	return 0
}
